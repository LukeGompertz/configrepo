# Remove Apple warning that prompts the user to switch to zsh
export BASH_SILENCE_DEPRECATION_WARNING=1

# Set prompts
PS1='\[\e[0;32m\]\w\$ \[\e[0m\]'
PS2='\[\e[0;32m\]> \[\e[0m\]'

# Set aliases
alias ll='ls -F'
alias sizes='du -hd 1'

# Set up alias for bare configuration repo
alias configrepo='/usr/bin/git --git-dir=$HOME/.configrepo/ --work-tree=$HOME'

# Function to enter a directory and list its contents
function cl {
cd "$1"
ls
}

# Function to move a file to the regular trash folder
function trash {
mv "$@" ~/.Trash/
}