<h1>Read Me<h1/>

This repository is for quick maintenance and backup of configuration files in a $HOME directory or its subdirectories.

To achieve this, a bare repository is used (i.e. the contents of .git/ are stored directly in the repo folder and there
is no working space). This is because the actual dot files need to be in specific locations in the file system hierarchy.

For ease of use, a simple alias is included in the .bash_profile. This alias, "configrepo", can be used just like the "git" command.

This idea for this setup came from [this article](https://www.atlassian.com/git/tutorials/dotfiles). Please read there for more information on how to setup something similar or to convert an existing dotfiles repo to this method.
